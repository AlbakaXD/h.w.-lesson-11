#include "threads.h"
#include <iostream>
#include <thread>


bool isPrime(int n)
{
    // Corner case
    if (n <= 1)
        return false;

    // Check from 2 to n-1
    for (int i = 2; i < n; i++)
        if (n % i == 0)
            return false;

    return true;
}

void I_Love_Threads()
{
	std::cout << "I love Threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread loveThread(I_Love_Threads);
	loveThread.join();
}

void printVector(std::vector<int> primes)
{
    for (int i = 0; i < primes.size(); i++)
    {
        std::cout << primes[i] << std::endl;
    }
}

void getPrimes(int begin, int end, std::vector<int> &primes)
{
    for (int i = begin; i <= end; i++)
    {
        if (isPrime(i))
        {
            primes.push_back(i);
        }
    }
}

std::vector<int> callGetPrimes(int begin, int end)
{
    clock_t time_req = clock();
    std::vector<int> primeVector;
    std::thread primeThread(getPrimes,begin, end, std::ref(primeVector));
    primeThread.join();
    std::cout << float(clock() - time_req) / CLOCKS_PER_SEC << "\n " << std::endl;

    return primeVector;
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
    std::vector<int> primeVector;
    getPrimes(begin, end, primeVector);
    for (int i = 0; i < primeVector.size(); i++)
    {
        file << primeVector[i] << std::endl;
    }
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
    clock_t time_req = clock();
    int thingy = (end - begin) / N;
    int thisEnd = thingy;
    std::ofstream openedFile(filePath);
    std::vector<std::thread> threadVector;
    for (int i = 0; i < N; i++)
    {
        threadVector.emplace_back(writePrimesToFile, begin, thisEnd, std::ref(openedFile));
        begin = begin + thingy;
        thisEnd = thisEnd + thingy;
    }

    for (int i = 0; i < threadVector.size(); i++)
    {
        threadVector[i].join();
    }
    std::cout << float(clock() - time_req) / CLOCKS_PER_SEC << "\n " << std::endl;
}


